# README #

## Build
First compile the application
``` shell
    gradle build
    gradle test
```

## Run
Run the application. Pass multiple files as parameters
``` shell
    gradle bootRun -Pargs=file1.txt,file2.txt
```

You’re given 2 text files on local filesystem; each should contain a piece of text in UTF-8 (words separated by one or more whitespace characters). The solution should be able to gracefully reject input not conforming to these requirements.

Two threads simultaneously read each its own file and populate a shared cache. When they finish, a separate thread traverses the cache and prints out the following stats for each word, one line per word:
<word> <total occurrences> = <occurrences in file1> + <occurrences in file2>
where:
<word> - a word that has been seen in at least one of the files <total occurrences> - a number of times this word has been seen in both files together <occurrences in file#> - a number of times this word has been seen in the respective file
The output shall be ordered descending by <total occurrences>  (from the highest value to the lowest).

The application should be able to handle both small and very long input files (consider performance). While preparing the solution think about a small project that should be easy to review, build, run and validate for another developer. Don't treat it as a start of a much bigger application.
