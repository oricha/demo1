package com.example.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
class ValidatorServiceTest {

    @Test
    @DisplayName("Fail wrong filePath")
    void testEmptyFilePath() {
        Exception exception = assertThrows(Exception.class, () -> {
            ValidatorService.readFile(null);
        });
    }
}