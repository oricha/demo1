package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.example.demo.service.ValidatorService;

import java.io.File;

@SpringBootApplication
@Slf4j
public class DemoApplication {

	public static void main(String[] args) throws Exception {

		System.out.println("Run");
		System.out.println("reading files : "+ String.join(",", args));

//		You’re given 2 text files on local filesystem
		String[] args1 = new String[]{"file1.txt", "file1.txt"};
		String[] finalArgs = args1;
		Thread thread1 = new Thread(() -> {
			try {
				ValidatorService.readFile(finalArgs[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		Thread thread2 = new Thread(() -> {
			try {
				ValidatorService.readFile(finalArgs[1]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		thread1.start();
		thread2.start();

		try
		{
			thread1.join();
			thread2.join();
		} catch (Exception e)
		{
			System.out.println("Interrupted");
		}
		ValidatorService.orderAndPrint();

//		prints out the following stats for each word, one line per word:
//		<word> <total occurrences> = <occurrences in file1> + <occurrences in file2>
//		where:
//		<word> - a word that has been seen in at least one of the file
//		<total occurrences> - a number of times this word has been seen in both files together
//		<occurrences in file#> - a number of times this word has been seen in the respective file
//		The output shall be ordered descending by <total occurrences>  (from the highest value to the lowest).

		SpringApplication.run(DemoApplication.class, args);
	}

}
