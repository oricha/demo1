package com.example.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class ValidatorService {

    static ConcurrentHashMap<String, Integer> words = new ConcurrentHashMap<>(16, 0.9f, 2);

    public static void readFile(String filePath) throws Exception {

        if (StringUtils.isEmpty(filePath)) {
            throw new Exception("File Path invalid :" + filePath);
        }

        try (Scanner scanner = new Scanner(new FileReader(filePath))) {
            while (scanner.hasNext()) {
                String[] line = scanner.nextLine().split("\\s+");
                for (String newWord:  line ){
                    if(words.containsKey(newWord)){
                        log.info("contains word: " +newWord);
                        words.replace(newWord, words.get(newWord)+ 1);
                    } else{
                        log.info("not contains word: " +newWord);
                        words.put(newWord, 1);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        log.info("collection size " + words.size());
    }

    public static void orderAndPrint() throws IOException {

        List<Map.Entry<String, Integer>> list;
        list = new ArrayList<>(words.entrySet());
//        Sort collection by Value
        list.sort(Comparator.comparing(Map.Entry::getValue));
        log.info("list size " + list.size());
        FileWriter fileWriter = new FileWriter("output.txt");
        PrintWriter printWriter = new PrintWriter(fileWriter);
        for( Map.Entry<String, Integer> word: list){
            printWriter.printf(word.getKey() + " " + word.getValue() + System.lineSeparator());
        }
        printWriter.close();

        log.info("Output file: output.txt ");
    }
}
